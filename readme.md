# Build the dockerfile.
command : docker build -t darknet-docker .  #(darknet_docker is the name for the image. User can give their own name)

# Once the dockerfile is build, user can check the image by the following command, An image with the given name should appear.
command : sudo docker ps

# Allow external applications to connect to the host's X display  
command : xhost +   

#running the docker with interactive mode.
command : docker run -it --rm  DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix yolo3-docker:(version)

# In order to keep the image size small, the model weight file and the test objets( image, video, rtsp streams) should be mounted to the container. 
# Use mount flag to share a directory from the local machine with the container.

command : sudo docker run -it --gpus all --rm  -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --mount type=bind,source=(directory where your weights, cfg, data and test files are),target=/app_data yolo3-docker:(version)


# Once the container has been executed, user can follow instructions from : https://github.com/AlexeyAB/darknet, to run inference on file source.Example for inference on image through yolov3 following command can be used

command : ./darknet detector test cfg/coco.data yolov3.cfg yolov3.weights -ext_output dog.jpg

* Above are default paths. User should put the correct paths for the required files.




Note:
() -> text in brackets to be updated by users.


